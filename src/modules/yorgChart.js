import * as d3 from 'd3'
import 'd3-selection-multi';
import './yorgChart.css';

export class YorgChart{
	constructor(cfg){
		this.margin = cfg.margin;
		this.width = cfg.width || ( window.innerWidth - (this.margin.left + this.margin.right) );
        this.height = cfg.height || 2*this.width;
		this.nodeWidth = cfg.nodeWidth || this.width / 7;
		this.nodeHeight = cfg.nodeHeight || this.nodeWidth*1.25;
		this.data = cfg.data || {};
		this.chartContainer = cfg.chartContainer || d3.select('#yorgChart');
		this.animationDuration = cfg.animationDuration || 500;
		this.depthVisible = cfg.depthVisible || 3;
		this.cascadeIn = cfg.cascadeIn || 1;
		cfg.initActions.forEach((action) => {
			this[action]();
		})
	}

	addChild(){
		console.log('adding a child in 2 seconds...')
		setTimeout(() => {
    		this.data.children.push({
    		  name: 'A new child!'
    		})
    		console.log('data now', this.data)
		}, 2000)
	}

	buildTree(){
	    this.tree = d3.tree().nodeSize( [ this.nodeWidth, this.nodeHeight ] );
    	this.treeLink = d3.linkVertical().x(d => d.x).y(d => d.y);
    	this.chartSVG();
	}

	chartSVG(){
    	this.root = d3.hierarchy(this.data);
		this.root.x0 = 0;
    	this.root.y0 = 0;
    	this.root.descendants().forEach((d, i) => {
        	d.id = i;
        	d._children = d.children;
			// Start collapsed then expand (uncomment for effect)
			d.children = null;
			setTimeout(() => {
				if( d.depth < this.depthVisible ) d.children = d._children;
				this.update(d);
			}, this.animationDuration * (d.depth+1) * this.cascadeIn);
    	});

    	this.svg = this.chartContainer.append('svg')
			.attrs({"class": "chart-container",
				"viewBox": [ // svg viewbox... dynamically updates
		        	-this.width / 2, // min-x
		        	-this.margin.top, // min-y
		        	this.width, // width
		        	this.height // height
		    	]
			})

    	this.gLink = this.svg.append("g")
			.attrs({
				"class": "tree-link"
			});

    	this.gNode = this.svg.append("g")
			.attrs({
				"cursor": "pointer"
			})

    	this.update(this.root);
	}

	update(source){
		const nodes = this.root.descendants().reverse();
		const links = this.root.links();

		// Compute the new tree layout.
		this.tree(this.root);

		let bottom = this.root;
		let top = this.root;
		this.root.eachBefore(node => {
			if (node.y < bottom.y) bottom = node;
			if (node.y > top.y) top = node;
		});

		const height = top.y - bottom.y + this.margin.top + this.margin.bottom + this.nodeHeight;
		const transition = this.svg.transition()
			.duration(this.animationDuration)
			.tween("resize", window.ResizeObserver ? null : () => () => this.svg.dispatch("toggle"))
			.attrs({
				height: height,
				viewBox: [
					-this.width / 2,
					-this.margin.top,
					this.width,
					height
				]
			});

		// Update the nodes…
		const node = this.gNode.selectAll("g")
			.data(nodes, d => d.id);

		// Enter any new nodes at the parent's previous position.
		const nodeEnter = node.enter().append("g")
			.on("click", d => {
				d.children = d.children ? null : d._children;
				this.update(d);
			})
			.attrs({
				transform: d => `translate(${source.x0},${source.y0})`,
				"class": "node-transparent bordered" // TBD
			});

		/* Person Container */
		const container_pct = 0.8, radius_pct = 0.05;
		const personContainerAttrs = {
			"width": this.nodeWidth * container_pct,
			"height": this.nodeHeight * container_pct,
			"y": 0,
			"rx": this.nodeWidth * radius_pct,
			"ry": this.nodeHeight * radius_pct,
			"fill": d => d._children ? "lightblue" : "white",
			"class": "person-container"
		}
		personContainerAttrs.x = -personContainerAttrs.width / 2;
		const personContainer = nodeEnter.append("rect")
			.attrs(personContainerAttrs)

		/* Headshot */
		const headshot_width_pct = 0.9, headshot_height_pct = 0.6, headshot_y_pct = 0.1;
		const headshotAttrs = {
			'xlink:href': d => {
				var headshot = d.data.headshot;
				if( !headshot ){
					if( d.data.gender ){
						headshot = `./images/avatar-${d.data.gender}.svg`;
					}
				}
				return headshot;
			},
			"x": personContainerAttrs.x + (1-headshot_width_pct)*personContainerAttrs.width/2,
			"y": headshot_y_pct*personContainerAttrs.height,
			'width': headshot_width_pct*personContainerAttrs.width,
			'height': headshot_height_pct*personContainerAttrs.height
		}
		const headshot = nodeEnter.append("image")
		    .attrs(headshotAttrs)
			.on("click", d => {
				console.log('you clicked on headshot', d.data)
				// window.open(`https://www.google.com/search?q=${d.data.name}`, '_blank')

				/* Add child example (sorta works) */
				// d.data.children.push({
	    		//   name: 'Newman Noob',
				//   title: 'Rookie',
				//   gender: "male"
	    		// })
				// this.root = d3.hierarchy(this.data);
				// this.update(d);
			})

		/* Name */
		const name_y_pct = 0.8;
		const nameAttrs = {
			y: name_y_pct * personContainerAttrs.height,
			"text-anchor": "middle",
			"font-size": Math.floor(0.1*personContainerAttrs.height), // Have font take up 10% of the container
			class: "person-name"
		};

		const name = nodeEnter.append("text")
			.attrs(nameAttrs)
		    .text(d => d.data.name)

		/* Title */
		const title_y_pct = 0.9;
		const titleAttrs = {
			y: title_y_pct * personContainerAttrs.height,
			"text-anchor": "middle",
			"font-size": Math.floor(0.1*personContainerAttrs.height) - 4,
			class: "person-title"
		}

		const title = nodeEnter.append("text")
			.attrs(titleAttrs)
		    .text(d => d.data.title)

		// Transition nodes to their new position.
		const nodeUpdate = node.merge(nodeEnter).transition(transition)
			.attrs({
				"transform": d => `translate(${d.x},${d.y})`,
				"class": "node-opaque bordered"
			});

		// Transition exiting nodes to the parent's new position.
		const nodeExit = node.exit().transition(transition).remove()
			.attrs({
				"transform": d => `translate(${source.x},${source.y})`,
				"fill-opacity": 0,
				"stroke-opacity": 0
			});

		// Update the links…
		const link = this.gLink.selectAll("path")
		  .data(links, d => d.target.id);

		// Enter any new links at the parent's previous position.
		const linkEnter = link.enter().append("path")
			.attrs({
				d: d => {
					const o = {x: source.x0, y: source.y0};
					return this.treeLink({
						source: o,
						target: o
					});
				}
			});

		// Transition links to their new position.
		link.merge(linkEnter).transition(transition)
			.attrs({
				d: d => {
					return this.treeLink({
						source: { x: d.source.x, y: d.source.y + personContainerAttrs.height },
						target: { x: d.target.x, y: d.target.y }
					})
				}
			});

		// Transition exiting nodes to the parent's new position.
		link.exit().transition(transition).remove()
			.attrs({
				d: d => {
					const o = {x: source.x, y: source.y};
					return this.treeLink({source: o, target: o});
				}
			});

		// Stash the old positions for transition.
		this.root.eachBefore(d => {
			d.x0 = d.x;
			d.y0 = d.y;
		});
	}

	async readFile(filename){
		this.data = await d3.json(filename).catch(console.error);
	}
}
