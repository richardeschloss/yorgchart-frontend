import { YorgChart } from './modules/yorgChart.js';
import * as myData from './data/generated.json';

const yorgChart = new YorgChart({
    data: myData,
    margin: {top: 10, right: 10, bottom: 10, left: 10},
    initActions: ['buildTree']
});
console.log('yorgChart', yorgChart);
