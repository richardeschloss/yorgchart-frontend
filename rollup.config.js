import json from 'rollup-plugin-json';
import resolve from 'rollup-plugin-node-resolve';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import postcss from 'rollup-plugin-postcss';

// `npm run build` -> `production` is true
// `npm run dev` -> `production` is false
const production = !process.env.ROLLUP_WATCH;

export default {
	onwarn: function (warning, warn) { // d3 has circular dependencies...
		if (warning.code === 'CIRCULAR_DEPENDENCY') return;
		warn(warning);
	},
	input: "src/app.js",
	output: {
		file: 'public/bundle.js',
		format: 'iife', // immediately-invoked function expression — suitable for <script> tags
		sourcemap: true
	},
	plugins: [
		json(), // Import JSON files
		postcss({ // Import css files
			plugins: []
		}),
		resolve(), // tells Rollup how to find modules...i.e., look in node_modules
		!production && livereload('public'), // Live reload (dev mode only)
		production && terser() // minify, but only in production
	]
};
