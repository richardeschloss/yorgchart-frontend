/* Requires */
const argv = require('minimist')(process.argv.slice(2))
const fs = require('fs');

class YorgChart_CLI{
	static genTree(cfg){
		const rootNode = {
			name: "Jazz",
			title: "Queen Bee",
			gender: "female",
			children: []
		}

		function appendChildren(level, node){
			if( level < cfg.nLevels ){
				level++;
				for( var i = 0; i < cfg.nChildren; i++ ){
					var addNode = {
						name: "Buzz",
						title: "Worker Bee",
						gender: "male",
						children: []
					};
					appendChildren(level, addNode)
					node.children.push(addNode);
				}
			}
		}

		appendChildren(1, rootNode);
		fs.writeFile(cfg.dest, JSON.stringify(rootNode), (err, resp) => {
			if( !err ){
				console.log('org chart written to:', cfg.dest)
			}
		});
	}
}

const commands = {
	genTree: {
		defaults: {
			'nLevels': 3,
			'nChildren': 3,
			'dest': './src/data/generated.json'
		}
	}
}

var commandArr = argv['_'];
if( commandArr.length == 1 ){
	var action = argv['_'];
	if( action ){
		var args = Object.keys(commands[action].defaults);
		var cmdCfg = {}
		args.forEach((arg) => {
			if( argv[arg] ){
				cmdCfg[arg] = argv[arg]
			} else {
				console.log('--' + arg, 'not specified. Using default:', commands[action].defaults[arg])
				cmdCfg[arg] = commands[action].defaults[arg];
			}
		})

		YorgChart_CLI[action](cmdCfg)
	}
} else {
	console.log('No commands specified. Possible commands:', Object.keys(commands))
}
